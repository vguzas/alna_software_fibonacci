﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            while (true)
            {
                Console.WriteLine("Iveskite fibonacci eiliu skaiciu");
                int n = int.Parse(Console.ReadLine());

                int[,] fibArray = new int[n, n];

                //creating rows
                for (int i = 0; i < n; i++)
                {
                    //filling rows
                    for (int j = 0; j <= i; j++)
                    {
                        //assigning numbers to first two rows
                        if (i < 2)
                            fibArray[i, j] = 1;
                        //adding 1 to row edges
                        else if (j == 0 || j == i)
                            fibArray[i, j] = 1;
                        //generating array
                        else
                        {
                            fibArray[i, j] = fibArray[(i - 1), j] + fibArray[(i - 1), (j - 1)];
                        }
                        //write single element
                        Console.Write(fibArray[i, j] + " ");
                    }
                    Console.WriteLine();
                }
                Console.WriteLine();
            }
            

            Console.ReadLine();
        }
    }
}
